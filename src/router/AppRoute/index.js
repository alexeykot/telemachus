import React, { PureComponent } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';


import MainContainer from '../../components/MainContainer';
import LoginPage from '../../pages/LoginPage';
import App from '../../App';
import ProtectedRoute from '../ProtectedRoute';
import { connect } from 'react-redux';
import MainPage from '../../pages/MainPage';
import ReportsPage from '../../pages/ReportsPage';


class AppRouter extends PureComponent {

  render() {
    const { authenticated } = this.props;
    return (
      <MainContainer>
        <Switch>
          <ProtectedRoute path="/main" authenticated={authenticated} component={MainPage} />
          <ProtectedRoute path="/reports/:type/:id" authenticated={authenticated} component={ReportsPage} />
          <Route path="/" component={LoginPage} />
        </Switch>
      </MainContainer>
    );
  }
}

const mapStateToProps = ({auth}) => ({
  authenticated: auth.authenticated,
});

export default withRouter(connect(mapStateToProps, null)(AppRouter));
