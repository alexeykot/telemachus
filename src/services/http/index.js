import axios from 'axios';
import TokenStorage from '../storage/token';

const baseURL = 'http://telemachustest5-001-site1.ctempurl.com/api'

const http = axios.create({
  withCredentials: false,
  baseURL,
  headers: {'Content-Type': 'application/json'},
});

http.interceptors.request.use(
  async request => {
    const token = TokenStorage.get();
    const newRequest = {...request};
    if (token) {
      newRequest.headers.Authorization = `Bearer ${token}`;
    }
    return newRequest;
  },
  error => Promise.reject(error),
);

const urlsWithTimezone = ['events/facts', 'events/current', 'events/fact', 'events/facts', 'admin/facts'];

http.interceptors.request.use((config) => {
  urlsWithTimezone.forEach(url => {
    if (config.url.toLowerCase().includes(url)) {
      config.params = {...config.params, timezoneOffset: new Date().getTimezoneOffset()}
    }
  })
  return config
});

export default http;
