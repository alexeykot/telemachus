import Storage from './index';

class DateStorage extends Storage {
  constructor() {
    super('DateFrom');
  }
}

export default new DateStorage();
