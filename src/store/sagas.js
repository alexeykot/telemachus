import {all, fork} from 'redux-saga/effects';

import * as authWatchers from './auth';
import * as reportsWatchers from './reports';

export default function* root() {
  yield all([
    fork(authWatchers.watchSignIn),
    fork(authWatchers.watchLogOut),
    fork(reportsWatchers.watchGetNoonReports),
    fork(reportsWatchers.watchGetNoonFilter),
    fork(reportsWatchers.watchUpdateNoonReports),
    fork(reportsWatchers.watchGetVessels),
    fork(reportsWatchers.watchGetConditionOptions),
    fork(reportsWatchers.watchGetNotCurrentConditionOptions)
  ]);
}
