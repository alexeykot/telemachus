import {
  call,
  takeEvery,
  put,
} from 'redux-saga/effects';
import http from '../../services/http';
import TokenStorage from '../../services/storage/token';
import UserStorage from '../../services/storage/user';
import DateStorage from '../../services/storage/date';
import SkipIdsStorage from '../../services/storage/skipIds';

const SIGNIN_REQUEST = 'auth/SIGNIN_REQUEST';
const SIGNIN_SUCCESS = 'auth/SIGNIN_SUCCESS';
const SIGNIN_FAILURE = 'auth/SIGNIN_FAILURE';

const LOGOUT_REQUEST = 'auth/LOGOUT_REQUEST';
const LOGOUT_SUCCESS = 'auth/LOGOUT_SUCCESS';
const LOGOUT_FAILURE = 'auth/LOGOUT_FAILURE';


export const initialState = {
  authLoading: false,
  authenticated: false,
  user: {},
  authError: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SIGNIN_REQUEST:
      return {...state, authLoading: true, authError: false};
    case SIGNIN_SUCCESS:
      return {
        ...state,
        authLoading: false,
        authenticated: true,
        user: action.user,
        authError: false
      };
    case SIGNIN_FAILURE:
      return {...state, authLoading: false, authError: true};

    case LOGOUT_REQUEST:
      return {...initialState}
    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestSignIn = payload => ({
  type: SIGNIN_REQUEST,
  payload,
});

export const requestLogout = () => ({
  type: LOGOUT_REQUEST,
});

// <<<WORKERS>>>
function* signIn({payload}) {
  try {
    const {data} = yield call(http.post, '/login/token', payload);
    console.log('signIn-data', data);
    yield call(TokenStorage.save, data.token);
    yield call(UserStorage.save, data);
    yield put({
      type: SIGNIN_SUCCESS,
      user: data,
    });
  } catch (error) {
    console.log('signIn-error', error);
    yield put({
      type: SIGNIN_FAILURE,
    });
  }
}

function* logout() {
  try {
    yield call(TokenStorage.delete);
    yield call(UserStorage.delete);
    yield call(DateStorage.delete);
    yield call(SkipIdsStorage.delete);
    yield put({
      type: LOGOUT_SUCCESS,
    });
  } catch (error) {
    console.log('logout-error', error);
  }
}

// <<<WATCHERS>>>
export function* watchSignIn() {
  yield takeEvery(SIGNIN_REQUEST, signIn);
}

export function* watchLogOut() {
  yield takeEvery(LOGOUT_REQUEST, logout);
}
