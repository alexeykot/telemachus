import {combineReducers} from 'redux';

import auth from './auth';
import reports from './reports';

const rootReducer = combineReducers({
  auth,
  reports,
});

export default rootReducer;
