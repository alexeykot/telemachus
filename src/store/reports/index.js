import {
  call,
  takeEvery,
  put,
  select,
} from 'redux-saga/effects';
import {PAGE_SIZE} from '../../constants';
import http from '../../services/http';

const GET_NOON_REPORTS_REQUEST = 'reports/GET_NOON_REPORTS_REQUEST';
const GET_NOON_REPORTS_SUCCESS = 'reports/GET_NOON_REPORTS_SUCCESS';
const GET_NOON_REPORTS_FAILURE = 'reports/GET_NOON_REPORTS_FAILURE';

const GET_CURRENT_CONDITIONS_REQUEST = 'reports/GET_CURRENT_CONDITIONS_REQUEST';
const GET_CURRENT_CONDITIONS_SUCESS = 'reports/GET_CURRENT_CONDITIONS_SUCESS';
const GET_CURRENT_CONDITIONS_FAILURE = 'reports/GET_CURRENT_CONDITIONS_FAILURE';

const GET_NOT_CURRENT_CONDITIONS_REQUEST = 'reports/GET_NOT_CURRENT_CONDITIONS_REQUEST';
const GET_NOT_CURRENT_CONDITIONS_SUCESS = 'reports/GET_NOT_CURRENT_CONDITIONS_SUCESS';
const GET_NOT_CURRENT_CONDITIONS_FAILURE = 'reports/GET_NOT_CURRENT_CONDITIONS_FAILURE';

const GET_VESSELS_REQUEST = 'reports/GET_VESSELS_REQUEST';
const GET_VESSELS_SUCESS = 'reports/GET_VESSELS_SUCESS';
const GET_VESSELS_FAILURE = 'reports/GET_VESSELS_FAILURE';

const UPDATE_NOON_REPORTS_REQUEST = 'reports/UPDATE_NOON_REPORTS_REQUEST';
const UPDATE_NOON_REPORTS_SUCCESS = 'reports/UPDATE_NOON_REPORTS_SUCCESS';
const UPDATE_NOON_REPORTS_FAILURE = 'reports/UPDATE_NOON_REPORTS_FAILURE';

const GET_NOON_FILTER_REQUEST = 'reports/GET_NOON_FILTER_REQUEST';
const GET_NOON_FILTER_SUCCESS = 'reports/GET_NOON_FILTER_SUCCESS';
const GET_NOON_FILTER_FAILURE = 'reports/GET_NOON_REPORTS_FAILURE';

export const initialState = {
  reportsLoading: false,
  updateLoading: false,
  noonReports: [],
  noonFilter: [],
  vessels: [],
  conditionsOptions: [],
  portsOptions: [],
  cargoesOptions: [],
  optionsLoading: false,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_NOON_REPORTS_REQUEST:
      return {...state, reportsLoading: true};
    case GET_NOON_REPORTS_SUCCESS:
      return {
        ...state,
        reportsLoading: false,
        noonReports: action.noonReports,
      };
    case GET_NOON_REPORTS_FAILURE:
      return {...state, reportsLoading: false};
    
    case GET_CURRENT_CONDITIONS_REQUEST:
      return {...state, optionsLoading: true};
    case GET_CURRENT_CONDITIONS_SUCESS:
      return {...state ,optionsLoading: false, conditionsOptions: action.currentCondition, portsOptions: action.ports, cargoesOptions: action.cargoes };
    case GET_CURRENT_CONDITIONS_FAILURE:
      return {...state, optionsLoading: false};

    case GET_NOT_CURRENT_CONDITIONS_SUCESS:
      return {...state, conditionsOptions: action.currentCondition};

    case UPDATE_NOON_REPORTS_REQUEST:
      return {...state, updateLoading: true};
    case UPDATE_NOON_REPORTS_SUCCESS:
      return {
        ...state,
        noonReports: action.noonReports,
        updateLoading: false,
      };
    case UPDATE_NOON_REPORTS_FAILURE:
      return {...state, updateLoading: false};
    
    case GET_NOON_FILTER_REQUEST:
        return {...state, reportsLoading: true};
    case GET_NOON_FILTER_SUCCESS:
        return {
          ...state,
          reportsLoading: false,
          noonFilter: action.noonFilter,
        };
    case GET_NOON_FILTER_FAILURE:
        return {...state, reportsLoading: false};
    
    case GET_VESSELS_REQUEST:
      return {...state};
    case GET_VESSELS_SUCESS:
      return {...state, vessels: action.vessels};

    default:
      return state;
  }
}

// <<<ACTIONS>>>
export const requestGetNoonReports = payload => ({
  type: GET_NOON_REPORTS_REQUEST,
  payload,
});

export const requestGetConditionOptions = () => ({
  type: GET_CURRENT_CONDITIONS_REQUEST,
});

export const requestGetNotCurrentConditionOptions = (condition) => ({
  type: GET_NOT_CURRENT_CONDITIONS_REQUEST,
  condition,
});

export const requestUpdateNoonReports = payload => ({
  type: UPDATE_NOON_REPORTS_REQUEST,
  payload,
});

export const requestGetNoonFilter = () => ({
  type: GET_NOON_FILTER_REQUEST,
});

export const requestGetVessels = () => ({
  type: GET_VESSELS_REQUEST,
});


const formGetNoonUrl = (payload) => {
  let url = '';
  if (payload?.page) {
    url += `?page=${payload.page}&pageSize=${PAGE_SIZE}`
  }
  return url;
}

// <<<WORKERS>>>
function* getNoonReports({payload}) {
  try {
    const user = yield select(state => state.auth.user);
    let data;
    if (user.role === 'Basic') {
      const {data: fetchData} = yield call(http.post, `/Events/facts${formGetNoonUrl(payload)}`, {
        EventTypeIds: [],
        EventStatuses: payload?.eventStatuses || [],
        DateFrom: payload?.dateFrom || null,
        DateTo: payload?.dateTo || null,
    });
      data = fetchData;
    } else {
      const {data: fetchData} = yield call(http.get, `/admin/facts${formGetNoonUrl(payload)}`);
      data = fetchData;
    }
    yield put({
      type: GET_NOON_REPORTS_SUCCESS,
      noonReports: data,
    });
  } catch (error) {
    console.log('err', error);
    yield put({
      type: GET_NOON_REPORTS_FAILURE,
    });
  }
}

const getUpdateUrl = (payload) => {
  if (!payload) {
    return '';
  }
  if (payload.pageNumber !== undefined && payload.vesselName !== undefined) {
    return `?userId=${payload.vesselName}&page=${payload.pageNumber}&pageSize=${PAGE_SIZE}`;
  }
  if (payload.pageNumber !== undefined) {
    return `?page=${payload.pageNumber}&pageSize=${PAGE_SIZE}`;
  }
  if (payload.vesselName !== undefined) {
    return `?userId=${payload.vesselName}`;
  }
  return '';
}

function* getConditionOptions() {
  try {
    const {data: currentConditionData} = yield call(http.get, '/eventType/currentCondition');
    const {data: portsData} = yield call(http.get, '/events/ports ');
    const {data: cargoesData} = yield call(http.get, '/events/cargoes ');
    yield put({
      type: GET_CURRENT_CONDITIONS_SUCESS,
      currentCondition: currentConditionData,
      ports: portsData,
      cargoes: cargoesData,
    });
  } catch (err) {
    yield put({
      type: GET_CURRENT_CONDITIONS_FAILURE,
    });
  }
}

function* getNotCurrentConditionOptions({condition}) {
  try {
    const {data: currentConditionData} = yield call(http.get, `/EventType/condition/${condition.conditionId}/${condition.conditionKey}/voyage/${condition.voyageId}`);
    // const {data: portsData} = yield call(http.get, '/events/ports ');
    // const {data: cargoesData} = yield call(http.get, '/events/cargoes ');
    console.log('currentConditionDatacurrentConditionData', currentConditionData);
    yield put({
      type: GET_NOT_CURRENT_CONDITIONS_SUCESS,
      currentCondition: currentConditionData,
    });
  } catch (err) {
    yield put({
      type: GET_NOT_CURRENT_CONDITIONS_FAILURE,
    });
  }
}

const formUpdateNoonUrl = (payload) => {
  let url = '';
  if (payload?.pageNumber) {
    url += `?page=${payload.pageNumber}&pageSize=${PAGE_SIZE}`;
  }
  if (payload?.vesselName) {
    url += `&userId=${payload.vesselName}`;
  }
  return url;
}

function* updateNoonReports({payload}) {
  try {
    const user = yield select(state => state.auth.user);
    let data;
    if (user.role === 'Basic') {
      const {data: fetchData} = yield call(http.post, `/Events/facts${formUpdateNoonUrl(payload)}`, {
        EventTypeIds: payload?.EventTypeIds?.length ? payload.EventTypeIds :  [],
        EventStatuses: payload?.EventStatuses?.length ? payload.EventStatuses :  [],
        DateFrom: payload?.DateFrom || null,
        DateTo: payload?.DateTo || null,
      });
      data = fetchData;
    } else {
      const {data: fetchData} = yield call(http.get, `/admin/facts${getUpdateUrl(payload)}`);
      data = fetchData;
    }
    yield put({
      type: UPDATE_NOON_REPORTS_SUCCESS,
      noonReports: data,
    });
  } catch (error) {
    yield put({
      type: UPDATE_NOON_REPORTS_FAILURE,
    });
  }
}


function* getNoonFilter() {
  try {
    const {data} = yield call(http.get, '/events/types');
    yield put({
      type: GET_NOON_FILTER_SUCCESS,
      noonFilter: data,
    });
  } catch (error) {
    yield put({
      type: GET_NOON_FILTER_FAILURE,
    });
  }
}

function* getVessels() {
  try {
    const {data} = yield call(http.get, '/admin/vessels');
    console.log('data', data);
    yield put({
      type: GET_VESSELS_SUCESS,
      vessels: data,
    })
  } catch (error) {
    console.log('error', error);
  }
}

// <<<WATCHERS>>>
export function* watchGetNoonReports() {
  yield takeEvery(GET_NOON_REPORTS_REQUEST, getNoonReports);
}

export function* watchUpdateNoonReports() {
  yield takeEvery(UPDATE_NOON_REPORTS_REQUEST, updateNoonReports);
}

export function* watchGetNoonFilter() {
  yield takeEvery(GET_NOON_FILTER_REQUEST, getNoonFilter);
}

export function* watchGetVessels() {
  yield takeEvery(GET_VESSELS_REQUEST, getVessels);
}

export function* watchGetConditionOptions() {
  yield takeEvery(GET_CURRENT_CONDITIONS_REQUEST, getConditionOptions);
}
export function* watchGetNotCurrentConditionOptions() {
  yield takeEvery(GET_NOT_CURRENT_CONDITIONS_REQUEST, getNotCurrentConditionOptions);
}
