import styled from 'styled-components';

export const Label = styled.p`
  font-weight: 500;
  margin: 10px 0;
`;

export const CloseButton = styled.button`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1111;
`;

export const ButtonsContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

export const ConfirmButton = styled.button`
    width: 50px;
    height: 20px;
    display: flex;
  justify-content: center;
  align-items: center;
`;