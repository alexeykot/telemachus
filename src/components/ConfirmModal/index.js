import React from 'react';
import Modal from 'react-modal';
import { createModal } from 'react-modal-promise'

import {ReactComponent as CloseIcon} from '../../assets/icons/close.svg';

// import moment from 'moment';

import * as S from './styled';


const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: 'auto',
    width: 400,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'scroll',
    maxHeight: 400,
  }
};

const CofirmModal = ({ isOpen, onResolve, onReject, type, data }) => {

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={() => onResolve(false)}
      style={customStyles}
      contentLabel="Confirmation Modal"
    >
      <div style={{maxHeight: 600, overflowY: 'scroll'}}>
      <S.CloseButton onClick={() => onResolve(false)}><CloseIcon /></S.CloseButton>
      <S.Label style={{color: type === 'error' ? 'red' : 'black'}}>{data.error || data.info}</S.Label>
      <S.ButtonsContainer>
        <S.ConfirmButton onClick={() => onResolve(true)}>Ok</S.ConfirmButton>
      </S.ButtonsContainer>
      </div>
  </Modal>
  )
}


export default createModal(CofirmModal);
