import styled from 'styled-components';

export const Container = styled.div`
  margin-left: 20px;
  position: relative;
`;

export const Label = styled.div`
  position: absolute;
  top: -20px;
`;

export const Input = styled.input`
  width: 200px;
  color: black;
  font-size: 14px;
  border-color: #CCCCCC;
  border-radius: 4px;
  border-width: 1px;
  border-style: solid;
  height: 100%;
`;