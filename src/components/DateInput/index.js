import React from 'react';

import * as S from './styled';

const DateInput = ({label = '', onChange, value}) => {
  return (
    <S.Container>
      <S.Label>{label}</S.Label>
      <S.Input value={value} onChange={onChange} type="date" />
    </S.Container>
  )
}

export default DateInput;