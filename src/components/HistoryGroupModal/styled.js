import styled from 'styled-components';

export const Label = styled.div`
  margin: 10px 0;
  font-weight: bold;
`;

export const CloseButton = styled.button`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1111;
`;

export const GroupName = styled.h3`
  margin-bottom: 20px;
`;

export const Value = styled.p``;

export const InputContainer = styled.div`
  min-width: 200px;
  min-height: 60px;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
`;