import React, { Fragment } from 'react';
import Loader from "react-loader-spinner";
import {connect} from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import ReactPaginate from 'react-paginate';
import DropDown from 'react-dropdown';
import Select from 'react-select';

import DateStorage from '../../services/storage/date';
import SkipIdsStorage from '../../services/storage/skipIds';
import add from '../../assets/images/Add.ico';
import synchronize from '../../assets/images/Synchronize.ico';

import {PAGE_SIZE} from '../../constants';
import {requestGetNoonReports, requestUpdateNoonReports, requestGetVessels, requestGetConditionOptions, requestGetNotCurrentConditionOptions} from '../../store/reports';

import * as S from './styled';
import 'react-dropdown/style.css';

import Table from './components/Table';
import http from '../../services/http';
import AddModal from './components/AddModal';
import DateInput from '../DateInput';
import confirmModal from '../ConfirmModal';

const selectAllOption = {
  value: "<SELECT_ALL>",
  label: "All Items"
};

const STATUSES_OPTIONS = [
  {
    label: 'InProgress',
    value: 1,
  },
  {
    label: 'Completed',
    value: 2,
  },
  {
    label: 'Rejected',
    value: 3,
  },
  {
    label: 'Approved',
    value: 4,
  },
]
class Conditions extends React.PureComponent {

  state = {
    typeOptions: [],
    selectedTypeOptions: [],
    selectedStatusOptions: [],
    selectedVessel: {
      label: 'All',
      value: null,
    },
    selectedConditionOption: null,
    selectedPortOption: {
      label: 'No Port',
      value: null,
    },
    selectedCargoOption: {
      label: 'No Cargo',
      value: null,
    },
    parcelRows: [
      {
        size: '1',
        quantity: '1',
      }
    ],
    selectedPage: 1,
    chosenCondition: '',
    chosenRow: undefined,
    isModalOpen: false,
    dateTime: '',
    comment: '',
    terminal: '',
    addLoading: false,
    isCurrentCondition: false,
    condition: {},
    dateFrom: DateStorage.get() || moment().subtract(1, 'month').format('YYYY-MM-DD'), //2022-04-05
    dateTo: null,
    checked: SkipIdsStorage.get() || [],
    isOpenedFromMain: false,
  }

  componentDidMount() {
    this.props.requestGetNoonReports({
      page: 1,
      dateFrom: this.state.dateFrom,
      dateTo: this.state.dateTo,
    });
    this.props.requestGetVessels();
    this.getTypesOptions();
  }

  onRefresh = () => {
    const {selectedVessel} = this.state;
    this.props.requestUpdateNoonReports({
      pageNumber: this.state.selectedPage,
      vesselName: selectedVessel.value ? selectedVessel.value : undefined,
      DateFrom: this.state.dateFrom,
      DateTo: this.state.dateTo,
      EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
    });
    this.props.requestGetVessels();
  };

  onCheckboxChange = (event) => {
    const checked = [...this.state.checked];
    if (checked.includes(event.id)) {
      this.setState({checked: checked.filter(e => e !== event.id)})
    } else {
      checked.push(event.id);
      this.setState({checked: [...checked]});
    }
  }

  handlePageClick = (data) => {
    const {selectedVessel} = this.state;
    let selected = data.selected;
    this.setState({selectedPage: selected + 1});
    this.setState({chosenCondition: '', chooseRow: undefined});
    this.props.requestUpdateNoonReports({
      pageNumber: selected + 1,
      vesselName: selectedVessel.value ? selectedVessel.value : undefined,
      DateFrom: this.state.dateFrom,
      DateTo: this.state.dateTo,
      EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
    });
  };

  formatDropOptions = () => {
    const {vessels} = this.props;
    const formatted = vessels.map(vessel => ({
      value: vessel.id,
      label: vessel.name,
    }))
    return [
      {
        label: 'All',
        value: null,
      },
      ...formatted
    ];
  }

  formatConditionDropOptions = () => {
    const {conditionsOptions} = this.props;
    const formatted = conditionsOptions.map(vessel => ({
      value: vessel.id,
      label: vessel.name,
    }))
    return formatted;
  }

  formatPortsDropOptions = () => {
    const {portsOptions} = this.props;
    const formatted = portsOptions.map(port => ({
      value: port.id,
      label: port.name,
    }))
    return formatted;
  }

  formatCargoesDropOptions = () => {
    const {cargoesOptions} = this.props;
    const formatted = cargoesOptions.map(cargo => ({
      value: cargo.id,
      label: cargo.name,
    }))
    return formatted;
  }

  onDropChange = (e) => {
    if (e.value === null) {
      this.props.requestUpdateNoonReports({
        pageNumber: this.state.selectedPage,
        DateFrom: this.state.dateFrom,
        DateTo: this.state.dateTo,
        EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
      });
    }
  
    this.props.requestUpdateNoonReports({
      vesselName: e.value,
      DateFrom: this.state.dateFrom,
      DateTo: this.state.dateTo,
      EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
    });
    this.setState({selectedVessel: e, chosenCondition: '', chooseRow: undefined});
  }

  onConditionDropChange = (e, type) => {
    if (type === 'selectedConditionOption') {
      this.setState({terminal: '', selectedPortOption: {
        label: 'No Port',
        value: null,
      }, selectedCargoOption: {
        label: 'No Cargo',
        value: null,
      }});
    }
    this.setState({[type]: e});
  }

  chooseCondition = (item) => this.setState({chosenCondition: item});

  chooseRow = (item) => this.setState({chosenRow: item});

  resetModalState = () => this.setState({selectedConditionOption: null, selectedPortOption: null, selectedCargoOption: null, comment: '', dateTime: '', terminal: ''});

  createCondition = async () => {
    const {dateTime, selectedConditionOption, selectedPortOption, selectedCargoOption, comment, terminal, parcelRows, isCurrentCondition, condition} = this.state;
    this.setState({addLoading: true});
    let bodyFormData = new FormData();
    if (dateTime) {
      bodyFormData.append('Timestamp', dateTime);
    }
    if (selectedPortOption.value) {
      bodyFormData.append('PortId', selectedPortOption.value);
    }
    if (selectedCargoOption.value) {
      bodyFormData.append('CargoId', selectedCargoOption.value);
    }
    if (comment) {
      bodyFormData.append('Comment', comment);
    }
    if (terminal) {
      bodyFormData.append('Terminal', terminal);
    }

    if (selectedConditionOption.value === 90 || selectedConditionOption.value === 92) {
      parcelRows.forEach((row, index) => {
        bodyFormData.append(`CargoDetails[${index}].ParcelSize`, parseFloat(row.size));
        bodyFormData.append(`CargoDetails[${index}].ParcelQuantity`, parseFloat(row.quantity));
      })
    }

    if (isCurrentCondition) {
      const { data } = await http.post(`/events/current/${selectedConditionOption.value}`, bodyFormData, {
        headers: { "Content-Type": "multipart/form-data" }
      });
      if (data.error || data.info) {
        await confirmModal({
          type: data.error ? 'error' : 'info',
          data: data
        });
      }
    } else {
      await http.post(`/Events/${selectedConditionOption.value}/voyage/${condition.voyageId}/condition/${condition.conditionId}/${condition.conditionKey}`, bodyFormData, {
        headers: { "Content-Type": "multipart/form-data" }
      })
    }

  
    this.props.requestUpdateNoonReports({
      pageNumber: 1,
      DateFrom: this.state.dateFrom,
      DateTo: this.state.dateTo,
      EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
    });
    this.setState({chosenCondition: '', chooseRow: undefined, addLoading: false});
    this.closeAddModal();
    this.resetModalState();
    this.props.requestGetConditionOptions();
  }

  addRow = () => {
    const {parcelRows} = this.state;
    this.setState({parcelRows: [...parcelRows, {
      size: '1',
      quantity: '1',
    }]})
  }

  removeRow = (index) => {
    const {parcelRows} = this.state;
    _.remove(parcelRows, (n, ind) => {
      return ind === index;
    });
    this.setState({parcelRows: [...parcelRows]});
  }

  onParcelChange = (e, ind, type) => {
    const newParcels = this.state.parcelRows.map((r, index) => {
      if (index === ind) {
        return {
          ...r,
          [type]: e.target.value,
        }
      }
      return r;
    })
    this.setState({parcelRows: newParcels})
  };

  openAddModalFromIcon = () => {
      this.setState({isOpenedFromMain: true});
      this.openAddModal(true);
  };

  openAddModal = (isCurrentCondition = false, condition) => {
    this.setState({ isModalOpen: true, isCurrentCondition, condition })
    if (!isCurrentCondition) {
      this.props.requestGetNotCurrentConditionOptions(condition);
    } else {
      this.props.requestGetConditionOptions();
    }
  };

  closeAddModal = () => {
    this.setState({isModalOpen: false, isOpenedFromMain: false});
    this.resetModalState();
  }

  getTypesOptions = async () => {
    const {data} = await http.get('/EventType/All');
    this.setState({typeOptions: [...data.map(i => ({label: i.name, value: i.id}))]});
  }

  onApplyFilter = () => {
    const {selectedVessel, selectedPage} = this.state;
    DateStorage.save(this.state.dateFrom);
    this.props.requestUpdateNoonReports({
      pageNumber: selectedPage,
      vesselName: selectedVessel.value ? selectedVessel.value : undefined,
      EventTypeIds: this.state.selectedTypeOptions.length ? this.state.selectedTypeOptions.map(i => i.value) : [],
      EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
      DateFrom: this.state.dateFrom,
      DateTo: this.state.dateTo,
      EventIdsToSkip: this.state.checked,
    });
  };

  onDownload = async (type) => {
    DateStorage.save(this.state.dateFrom);
    SkipIdsStorage.save(this.state.checked);
    http.post(`/Events/facts/${type}`, {
      EventTypeIds: this.state.selectedTypeOptions.length ? this.state.selectedTypeOptions.map(i => i.value) : [],
      EventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
      DateFrom: this.state.dateFrom,
      DateTo: this.state.dateTo,
      EventIdsToSkip: this.state.checked,
    }, {
      responseType: 'blob'
    })
    .then((response) => {
        const href = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement('a');
        link.href = href;
        link.setAttribute('download', `report-${moment().format('DD-MM-YYYY/HH:mm')}.${type}`); //or any other extension
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    })
  }

  onSelect = (value, actionMeta, type) => {
    let prop = '';
    let options = [];
    if (type === 'types') {
      prop = 'selectedTypeOptions';
      options = this.state.typeOptions
    }
    if (type === 'statuses') {
      prop = 'selectedStatusOptions';
      options = STATUSES_OPTIONS
    }
    const { action, option } = actionMeta;
    if (action === "select-option" && option.value === selectAllOption.value) {
      this.setState({[prop]: options});
    } else {
      this.setState({[prop]: value});
    }
  };

  onDateChange = (value, type) => {
    this.setState({[type]: value});
  };

  render() {
    const {noonReports, reportsLoading, updateLoading, user, vessels} = this.props;
    return (
      <S.Container>
        <S.ButtonsContainer>
        <S.IconButton onClick={this.onRefresh}>
          <S.Icon src={synchronize} />
        </S.IconButton>
        {user.role === 'Basic' && (
          <S.IconButton onClick={this.openAddModalFromIcon}>
            <S.Icon src={add} />
          </S.IconButton>
        )}
        {user.role === 'Admin' && vessels.length ? (
          <DropDown
            value={this.state.selectedVessel}
            onChange={this.onDropChange}
            options={this.formatDropOptions()}
          />
        ) : null}
        </S.ButtonsContainer>
        {user.role === 'Basic' ? <Fragment>
          <S.DropdownsContainer>
            <Select
              placeholder="Select Event Type"
              styles={{
                container: (styles) => ({...styles, width: 300, zIndex: 1111}),
              }}
              onChange={(value, actionMeta) => this.onSelect(value, actionMeta, 'types')}
              options={[selectAllOption ,...this.state.typeOptions]}
              isMulti
              value={this.state.selectedTypeOptions}
            />
            <Select
              placeholder="Select Event Status"
              styles={{
                container: (styles) => ({...styles, marginLeft: 20, width: 300, zIndex: 11111}),
              }}
              onChange={(value, actionMeta) => this.onSelect(value, actionMeta, 'statuses')}
              value={this.state.selectedStatusOptions}
              options={[selectAllOption,...STATUSES_OPTIONS]}
              isMulti
            />
            <DateInput value={this.state.dateFrom} onChange={(e) => this.onDateChange(e.target.value, 'dateFrom')} label="Date From" />
            <DateInput value={this.state.dateTo} onChange={(e) => this.onDateChange(e.target.value, 'dateTo')} label="Date To" />
          </S.DropdownsContainer>
          <S.ButtonsContainer style={{paddingLeft: 0}}>
            <S.ApplyFilterButton style={{marginRight: 40}} onClick={this.onApplyFilter}>Apply</S.ApplyFilterButton>
            <S.ApplyFilterButton onClick={() => this.onDownload('txt')}>Download TXT</S.ApplyFilterButton>
            <S.ApplyFilterButton onClick={() => this.onDownload('pdf')}>Download PDF</S.ApplyFilterButton>
          </S.ButtonsContainer>
        </Fragment> : null}
        {this.state.isModalOpen && <AddModal
          onConditionDropChange={this.onConditionDropChange}
          selectedCargoOptions={this.state.selectedCargoOption}
          selectedPortOptions={this.state.selectedPortOption}
          selectedConditionOptions={this.state.selectedConditionOption}
          conditionsOptions={this.formatConditionDropOptions()}
          portOptions={this.formatPortsDropOptions()}
          cargoOptions={this.formatCargoesDropOptions()}
          date={this.state.dateTime}
          onDateChange={e => this.setState({dateTime: e.target.value})}
          onRequestClose={this.closeAddModal}
          isModalOpen={this.state.isModalOpen}
          createCondition={this.createCondition}
          comment={this.state.comment}
          terminal={this.state.terminal}
          setTerminal={(e) => this.setState({terminal: e})}
          setComment={(e) => this.setState({comment: e})}
          addLoading={this.state.addLoading}
          parcelRows={this.state.parcelRows}
          addRow={this.addRow}
          removeRow={this.removeRow}
          onParcelChange={this.onParcelChange}
          condition={this.state.condition}
          isOpenedFromMain={this.state.isOpenedFromMain}
        />}
        {updateLoading && <S.UpdateLoaderContainer><Loader type="Oval" color="#77acd9" /></S.UpdateLoaderContainer>}
        {!reportsLoading && noonReports.items ? (
          <>
            <Table
              checked={this.state.checked}
              onCheckboxChange={this.onCheckboxChange}
              setChosenRow={this.chooseRow}
              setChosen={this.chooseCondition}
              chosenRow={this.state.chosenRow}
              chosen={this.state.chosenCondition}
              isAdmin={user.role === 'Admin'}
              onAddPress={this.openAddModal}
              data={noonReports.items}
              portOptions={this.formatPortsDropOptions()}
              cargoOptions={this.formatCargoesDropOptions()}
              updateLoading={this.props.updateLoading}
              refetchData={() => this.props.requestGetNoonReports({
                dateFrom: this.state.dateFrom,
                dateTo: this.state.dateTo,
                eventStatuses: this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : [],
              })}
              dateFrom={this.state.dateFrom}
              dateTo={this.state.dateTo}
              eventStatuses={this.state.selectedStatusOptions.length ? this.state.selectedStatusOptions.map(i => i.value) : []}
            />
          </>
        ) : <S.LoaderContainer><Loader type="Oval" color="#77acd9" /></S.LoaderContainer>}
            <ReactPaginate
              previousLabel={'previous'}
              nextLabel={'next'}
              breakLabel={'...'}
              breakClassName={'break-me'}
              pageCount={noonReports.totalCount ? Math.ceil(noonReports.totalCount / PAGE_SIZE) : 1}
              onPageChange={this.handlePageClick}
              marginPagesDisplayed={2}
              pageRangeDisplayed={5}
              containerClassName={'pagination'}
              subContainerClassName={'pages pagination'}
              activeClassName={'active'}
            />
    </S.Container>
    )
  }
}

const mapStateToProps = ({reports, auth}) => ({
  noonReports: reports.noonReports,
  reportsLoading: reports.reportsLoading,
  updateLoading: reports.updateLoading,
  user: auth.user,
  vessels: reports.vessels,
  conditionsOptions: reports.conditionsOptions,
  portsOptions: reports.portsOptions,
  cargoesOptions: reports.cargoesOptions,
});

const mapDispatchToProps = {
  requestGetNoonReports,
  requestUpdateNoonReports,
  requestGetVessels,
  requestGetConditionOptions,
  requestGetNotCurrentConditionOptions
};

export default connect(mapStateToProps, mapDispatchToProps)(Conditions);