import React from 'react';
import Modal from 'react-modal';
import SelectDropDown from "react-dropdown-select";
import DropDown from 'react-dropdown';

import {ReactComponent as CloseIcon} from '../../../../assets/icons/close.svg';

import * as S from './styled';
import http from '../../../../services/http';

const customStyles = {
  content : {
    top: '50%',
    left: '70%',
    right: '0',
    bottom: '50%',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: 600,
    width: 600,
    display: 'flex',
    flexDirection: 'column',
  }
};

Modal.setAppElement('#root')

const AddModal = ({
  isModalOpen,
  onRequestClose,
  onConditionDropChange,
  conditionsOptions,
  portOptions,
  cargoOptions,
  selectedConditionOptions,
  selectedPortOptions,
  selectedCargoOptions,
  createCondition,
  onDateChange,
  comment,
  setComment,
  setTerminal,
  terminal,
  addLoading,
  parcelRows,
  addRow,
  removeRow,
  onParcelChange,
  condition,
  date,
  isOpenedFromMain = false,
}) => {

  const [current, setCurrent] = React.useState('');
  React.useEffect(() => {
    async function check() {
      const {data} = await http.get('/Events/CurrentCondition');
      setCurrent(data.condition);
    }
    check();
  }, [current]);

  const formattedCargoOptions = [
    {
      label: 'No Cargo',
      value: null,
    },
    ...cargoOptions,
  ];

  const formattedPortOptions = [
    {
      label: 'No Port',
      value: null,
    },
    ...portOptions,
  ];

  const isButtonDisabled = () => {
    if (isOpenedFromMain) {
      return addLoading || !selectedConditionOptions || !selectedConditionOptions.value
    }
    return !date || addLoading || !selectedConditionOptions || !selectedConditionOptions.value
  }

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      style={customStyles}
      contentLabel="Example Modal"
    >
      <S.CloseButton onClick={onRequestClose}><CloseIcon /></S.CloseButton>

      {/* CURRENT TITLE */}
      <S.TitleContainer>
        {condition ? <S.Label>{condition.isCurrentCondition ? current : condition.conditionName}</S.Label> : <S.Label>{current}</S.Label>}
      </S.TitleContainer>

      {/* DATE */}
      <S.Label>Choose date</S.Label>
      <input onChange={e => onDateChange(e)} type="datetime-local" />
      
      {/* Event type */}
      <S.Label>Choose Event type</S.Label>
      <SelectDropDown value={selectedConditionOptions} onChange={(e) => onConditionDropChange(e[0], 'selectedConditionOption')} options={conditionsOptions} />

      { //PORT
        selectedConditionOptions?.value === 53 ? 
          <>
            <S.Label>Choose Port</S.Label>
            <DropDown value={selectedPortOptions} onChange={(e) => onConditionDropChange(e, 'selectedPortOption')} options={formattedPortOptions} />
          </>
        : null
      }
      
      {/* Cargo */}
      {
         selectedConditionOptions?.value === 90 || selectedConditionOptions?.value === 91 || selectedConditionOptions?.value === 92 || selectedConditionOptions?.value === 93 ? 
          <>
            <S.Label>Choose Cargo</S.Label>
            <DropDown value={selectedCargoOptions} onChange={(e) => onConditionDropChange(e, 'selectedCargoOption')} options={formattedCargoOptions} />
          </>
         : null
      }
  
      {/* Comment */}
      <S.Label>Add Comment</S.Label>
      <S.Textarea value={comment} onChange={(e) => setComment(e.target.value)}>
        {comment}
      </S.Textarea>

      { //TERMINAL
        selectedConditionOptions?.value === 2 ? 
          <>
            <S.Label>Terminal</S.Label>
            <S.Textarea value={terminal} onChange={(e) => setTerminal(e.target.value)}>
              {terminal}
            </S.Textarea> 
          </>
        : null
      }

      { // CARGO DETAILS
        selectedConditionOptions?.value === 90 || selectedConditionOptions?.value === 92 ?
        <div style={{height: 200, overflow: 'scroll'}}>
          {parcelRows.map((row, index) => (
            <div style={{
              display: 'flex',
            }}>
              <div>
                <S.Label>Cargo parcel size</S.Label>
                <input value={row.size} onChange={(e) => onParcelChange(e, index, 'size')} id="number" type="number" step="0.1"></input>
              </div>
              <div style={{marginLeft: 20}}>
                <S.Label>Cargo parcel quantity</S.Label>
                <input value={row.quantity} onChange={(e) => onParcelChange(e, index, 'quantity')} id="number" type="number" step="0.1"></input>
              </div>
              {index === (parcelRows.length - 1) ? <S.AddRowButton onClick={addRow}>+</S.AddRowButton> : <S.AddRowButton onClick={() => removeRow(index)}>x</S.AddRowButton>}
            </div> 
          ))}
        </div>
        : null
      }
      <S.Button disabled={isButtonDisabled()} onClick={createCondition}>Create</S.Button>
  </Modal>
  )
}

export default AddModal;
