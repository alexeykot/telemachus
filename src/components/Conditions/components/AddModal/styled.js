import styled from 'styled-components';

export const Label = styled.div`
  margin: 10px 0;
  font-weight: bold;
`;

export const Textarea = styled.textarea`
  width: 100%;
  height: 100px;
  margin-top: 10px;
`;

export const Button = styled.button`
  height: 43px;
  width: 117px;
  background: #556080;
  border-radius: 4px;
  color: white;
  margin: auto;
  opacity: ${({disabled}) => disabled ? 0.2 : 1};
`;

export const CloseButton = styled.button`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const AddRowButton = styled.button`
  width: 26px;
  height: 26px;
  align-self: flex-end;
  margin-left: 20px;
`;

export const TitleContainer = styled.div`
  display: flex;
  background-color: #2ab7f5;
  width: 100%;
  height: 50px;
  margin-top: 10px;
  justify-content: center;
  align-items: center;
`;
