import styled from 'styled-components';

export const OptionContainer = styled.div`
  width: 100px;
  display: flex;
  justify-content: center;
  :hover {
    cursor: pointer;
    background-color: #556080;
    color: white;
  }
`;

export const OptionText = styled.span``;
