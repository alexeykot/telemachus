import React from 'react';

import { ContextMenu, MenuItem } from "react-contextmenu";

import * as S from './styled';

const Menu = ({id, options = ['Add'], onOptionClick = () => {}}) => (
  <ContextMenu id={id} style={{
    zIndex: 1111,
    width: '100px',
    backgroundColor: 'white',
    border: '1px solid #e2e2e2',
    borderRadius: '4px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  }}>
    <MenuItem onClick={onOptionClick} data={{foo: 'bar'}}>
      {options.map(opt => <S.OptionContainer>{opt} event</S.OptionContainer>)}
    </MenuItem>
  </ContextMenu>
);

export default Menu;