import styled from 'styled-components';

export const Label = styled.div`
  margin: 10px 0;
  font-weight: bold;
`;

export const Textarea = styled.textarea`
  width: 100%;
  height: 100px;
`;

export const AttachmentContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;

export const DeleteButton = styled.button`
  height: 50px;
`;

export const FileInput = styled.input`
  margin-bottom: 10px;
  margin-top: 10px;
`;


export const Button = styled.button`
  height: 43px;
  width: 117px;
  background: #556080;
  border-radius: 4px;
  color: white;
  margin: auto;
`;

export const CloseButton = styled.button`
  width: 20px;
  height: 20px;
  position: absolute;
  top: 10px;
  right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1111;
`;

export const AddRowButton = styled.button`
  width: 26px;
  height: 26px;
  align-self: flex-end;
  margin-left: 20px;
`;
