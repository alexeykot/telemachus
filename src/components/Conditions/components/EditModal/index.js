import React from 'react';
import Modal from 'react-modal';
import DropDown from 'react-dropdown';
import {ReactComponent as CloseIcon} from '../../../../assets/icons/close.svg';

import _ from 'lodash';
import * as S from './styled';
import http from '../../../../services/http';
import confirmModal from '../../../ConfirmModal';

import {requestUpdateNoonReports, requestGetConditionOptions} from '../../../../store/reports';

import { connect } from 'react-redux';

const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: 600,
    width: 600,
    display: 'flex',
    flexDirection: 'column',
  }
};

const cargoStatuses = [
  {
    label: 'None',
    value: 0,
  },
  {
    label: 'Loaded',
    value: 1,
  },
  {
    label: 'Ballast',
    value: 2,
  },
]

const EditModal = ({
  isModalOpen,
  onRequestClose,
  event,
  portOptions,
  cargoOptions,
  requestUpdateNoonReports,
  unSelectEvent,
  requestGetConditionOptions,
  dateTo,
  dateFrom,
  eventStatuses,
}) => {

  const [timestamp, setTimestamp] = React.useState(event.timestamp);
  const [cargoId, setCargoId] = React.useState();
  const [portId, setPortId] = React.useState();
  const [comment, setComment] = React.useState(event.comment);
  const [terminal, setTerminal] = React.useState(event.terminal);
  const [attachments, setAttachments] = React.useState(event.attachments);
  const [deleted, setDeleted] = React.useState([]);
  const [cargoDetails, setCargoDetails] = React.useState(event?.cargoDetails.length ? event?.cargoDetails : [{parcelQuantity: 1, parcelSize: 1}]);
  const [cargoStatus, setCargoStatus] = React.useState(event?.cargoStatus);
  const [ballast, setBallast] = React.useState(event?.ballastQuantity || 0);

  React.useEffect(() => {
    setAttachments(event.attachments);
    setTimestamp(event.timestamp);
    setComment(event.comment);
    setCargoId(event.cargo ? event.cargo.id : null);
    setPortId(event.port ? event.port.id : null);
  }, [event, event.attachments, event.timestamp, event.comment]);

  const onDelete = (index, id) => {
    deleted.push({
      index: index,
      id: id,
    });
    setDeleted(deleted);
    setAttachments(attachments.filter(a => a.id !== id));
  };
  const onSubmit = async () => {
    try {
    let bodyFormData = new FormData();
    if (timestamp) {
      bodyFormData.append('Timestamp', timestamp);
    }

    if (cargoId) {
      bodyFormData.append('CargoId', cargoId);
    }

    if (portId) {
      bodyFormData.append('PortId', portId);
    }

    if (comment) {
      bodyFormData.append('Comment', comment);
    }
    if (terminal) {
      bodyFormData.append('Terminal', terminal);
    }
    if (deleted.length) {
      for (let i = 0; i < deleted.length; i++) {
        bodyFormData.append(`RemoveFileIds[${deleted[i].index}]`, deleted[i].id)
      }
    };

    if (event?.eventTypeId === 90 || event?.eventTypeId === 91 || event?.eventTypeId === 92 || (event?.eventTypeId === 93 && cargoStatus === 1)) {
      for (let i = 0; i < cargoDetails.length; i++) {
        bodyFormData.append(`CargoDetails[${i}].ParcelSize`, parseFloat(cargoDetails[i].parcelSize));
        bodyFormData.append(`CargoDetails[${i}].ParcelQuantity`, parseFloat(cargoDetails[i].parcelQuantity));
      } 
    }
  
    if (event?.eventTypeId === 93) {
      bodyFormData.append('CargoStatus', cargoStatus);
    }

    if (event?.eventTypeId === 93 && cargoStatus === 2) {
      bodyFormData.append('BallastQuantity', ballast);
    }

    if (inputRef.files.length) {
      for (let x = 0; x < inputRef.files.length; x++) {
        bodyFormData.append('Files', inputRef.files[x]);;
      }
    };
    const { data } = await http.patch(`/events/fact/${event.id}`, bodyFormData, {
      headers: { "Content-Type": "multipart/form-data" }
    });
  
    if (data.error || data.info) {
      await confirmModal({
        type: data.error ? 'error' : 'info',
        data: data
      });
    }
    } catch (err) {
      console.log('eerrr', err);
    }
    onRequestClose();
    unSelectEvent();
    requestUpdateNoonReports({
      DateFrom: dateFrom,
      DateTo: dateTo,
      EventStatuses: eventStatuses,
    });
    requestGetConditionOptions();
  }

  const addRow = () => {
    setCargoDetails([...cargoDetails, {
      parcelSize: '1',
      parcelQuantity: '1',
    }])
  }

  const removeRow = (index) => {
    _.remove(cargoDetails, (n, ind) => {
      return ind === index;
    });
    setCargoDetails([...cargoDetails]);
  }

  const onParcelChange = (e, ind, type) => {
    const newDetails = cargoDetails.map((r, index) => {
      if (index === ind) {
        return {
          ...r,
          [type]: e.target.value,
        }
      }
      return r;
    })
    setCargoDetails(newDetails);
  };

  const formattedCargoOptions = [
    {
      label: 'No Cargo',
      value: null,
    },
    ...cargoOptions,
  ];

  const formattedPortOptions = [
    {
      label: 'No Port',
      value: null,
    },
    ...portOptions,
  ];

  let inputRef;
  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      style={customStyles}
      contentLabel="Example Modal"
    >
      <div style={{maxHeight: 600, overflowY: 'scroll'}}>
      <S.CloseButton onClick={onRequestClose}><CloseIcon /></S.CloseButton>

      {/* Timestamp */}
      <S.Label>Choose date</S.Label>
      <input onChange={e => setTimestamp(e.target.value)} value={timestamp} type="datetime-local" />

      {/* CARGO TYPE */}
      {
         (event?.eventTypeId === 90 || event?.eventTypeId === 91 || event?.eventTypeId === 92 || event?.eventTypeId === 93) ?
          <>
            <S.Label>Choose Cargo type</S.Label>
            <DropDown onChange={(e) => setCargoId(e.value)} value={formattedCargoOptions.find(c => c.value === cargoId)} options={formattedCargoOptions} />
          </>
         : 
          <>
            <S.Label>Cargo type</S.Label>
            <div>{formattedCargoOptions.find(c => c.value === cargoId)?.label === 'No Cargo' ? 'None' : formattedCargoOptions.find(c => c.value === cargoId)?.label}</div>
          </>
      }
  
      { //PORT TYPE
        event?.eventTypeId === 53 ?
          <>
            <S.Label>Choose Port type</S.Label>
            <DropDown onChange={(e) => setPortId(e.value)} value={formattedPortOptions.find(p => p.value === portId)} options={formattedPortOptions} />
          </> : 
          <>
            <S.Label>Port</S.Label>
            <div>{event?.port?.name || 'None'}</div>
          </>
      }
  
      {/* COMMENT */}
      <S.Label>Comment</S.Label>
      <S.Textarea value={comment} onChange={(e) => setComment(e.target.value)}>
        {comment}
      </S.Textarea>
      
      { //TERMINAL
        event?.eventTypeId === 2 ?
          <>
            <S.Label>Terminal</S.Label>
            <S.Textarea value={terminal} onChange={(e) => setTerminal(e.target.value)}>
              {terminal}
            </S.Textarea>
          </> : 
          <>
            <S.Label>Terminal</S.Label>
            <div>{event?.terminal || 'None'}</div>
          </>
      }
  
      { //CARGO STATUS
        event?.eventTypeId !== 93 ?
          <>
            <S.Label>Cargo Status</S.Label>
            <div>{cargoStatuses.find(s => s.value === event?.cargoStatus).label || 'None'}</div>
          </> :
          <>
            <S.Label>Choose Cargo Status</S.Label>
            <DropDown onChange={(e) => setCargoStatus(e.value)} value={cargoStatuses.find(c => c.value === cargoStatus)} options={cargoStatuses} />
          </>
      }

      { // BallastQuantity for 93
        (event?.eventTypeId === 93 && cargoStatus === 2) ?
          <>
            <S.Label>Ballast quantity</S.Label>
            <input
              min={0}
              onChange={(e) => setBallast(e.target.value)}
              value={ballast}
              id="number"
              type="number"
              step="0.1"
            />
          </>
      : null
      }

      { //CARGO DETAILS for 90/91/92 and 93 with choosed Loaded status
        (event?.eventTypeId === 90 || event?.eventTypeId === 91 || event?.eventTypeId === 92 || (event?.eventTypeId === 93 && cargoStatus === 1)) ? 
          <div style={{height: 200, overflowY: 'scroll'}}>
            {cargoDetails.map((detail, index) => (
              <div style={{
                display: 'flex',
              }}>
                <div>
                  <S.Label>Cargo parcel size</S.Label>
                  <input
                    onChange={(e) => onParcelChange(e, index, 'parcelSize')}
                    value={detail.parcelSize}
                    id="number"
                    type="number"
                    step="0.1"
                  />
                </div>
                <div style={{marginLeft: 20}}>
                  <S.Label>Cargo parcel quantity</S.Label>
                  <input
                    onChange={(e) => onParcelChange(e, index, 'parcelQuantity')}
                    value={detail.parcelQuantity}
                    id="number"
                    type="number"
                    step="0.1"
                  />
                </div>
                {index === (cargoDetails.length - 1) ? <S.AddRowButton onClick={addRow}>+</S.AddRowButton> : <S.AddRowButton onClick={() => removeRow(index)}>x</S.AddRowButton>}
              </div> 
            ))}
          </div> 
        : null
      }
    
      {/* FILES */}
      <S.FileInput
        ref={r => {
          inputRef = r;
        }}
        name="myFile"
        type="file"
        multiple
      />
      {(attachments && attachments.length) ? attachments.map((att, ind) => {
        return (
          <S.AttachmentContainer>
            <a target="_blank" href={`http://sunetos-001-site1.gtempurl.com/${att.url}`} download>{att.url.split('/')[2]}</a>
            <S.DeleteButton onClick={() => onDelete(ind, att.id)}>delete</S.DeleteButton>
          </S.AttachmentContainer>
        )
      }) : null}
  
      <S.Button disabled={!timestamp} onClick={onSubmit}>Submit</S.Button>
      </div>
  </Modal>
  )
}

const mapDispatchToProps = {
  requestUpdateNoonReports,
  requestGetConditionOptions,
};

export default connect(null, mapDispatchToProps)(EditModal);
