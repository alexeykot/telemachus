import React from 'react';
import Modal from 'react-modal';

import {ReactComponent as CloseIcon} from '../../../../assets/icons/close.svg';

import moment from 'moment';

import * as S from './styled';


const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: 'auto',
    width: 400,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'scroll',
    maxHeight: 400,
  }
};

const ConfirmationModal = ({
  isModalOpen,
  onRequestClose,
  onConfirm = () => {},
  event = {},
}) => {

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      style={customStyles}
      contentLabel="Confirmation Modal"
    >
      <div style={{maxHeight: 600, overflowY: 'scroll'}}>
      <S.CloseButton onClick={onRequestClose}><CloseIcon /></S.CloseButton>
      <S.Label>Are you sure you want to delete '{event.eventTypeName}' event from {event.timestamp ? moment(event.timestamp).format('DD.MM.YYYY HH:mm') : 'N/A'}?</S.Label>
      <S.ButtonsContainer>
        <S.ConfirmButton onClick={onConfirm}>Yes</S.ConfirmButton>
      </S.ButtonsContainer>
      </div>
  </Modal>
  )
}


export default ConfirmationModal;
