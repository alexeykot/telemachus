import React from 'react';
import moment from 'moment';

import { MenuItem, ContextMenuTrigger } from "react-contextmenu";
import _ from 'lodash';
import ContextMenu from '../ContextMenu';
import { ReactComponent as RightArrowIcon } from '../../../../assets/icons/right-arrow.svg';
import { ReactComponent as DownArrowIcon } from '../../../../assets/icons/arrow-down.svg';
import { ReactComponent as CollapseIcon } from '../../../../assets/icons/collapse.svg';
import { ReactComponent as EditIcon } from '../../../../assets/icons/edit.svg';

import * as S from './styled';
import ListOption from '../ListOption';
import EditModal from '../EditModal';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Fragment } from 'react';
import http from '../../../../services/http';
import ConfirmationModal from '../ConfirmationModal';
import confirmModal from '../../../ConfirmModal';

const approvalStatuses = {
  1: 'In progress',
  2: 'Completed',
  3: 'Rejected',
  4: 'Approved',
};

const approvalStatusesColors = {
  1: '255, 0, 0, 0.3',
  2: '0, 255, 0, 0.3',
  3: '0, 255, 0, 0.3',
  4: 'white',
};

class Table extends React.Component {
  state = {
    isEditModalOpen: false,
    selectedEvent: {},
    confirmationModalOpen: false,
    deletedEvent: {},

  }

  componentDidMount() {
    if (this.props.data.length) {
      const {setChosen} = this.props;
      setChosen(JSON.stringify(this.props.data[0]));
    }
  }

  componentDidUpdate(prevProps) {
    if ((this.props.updateLoading !== prevProps.updateLoading) && !this.props.updateLoading) {
      const {setChosen} = this.props;
      setChosen(JSON.stringify(this.props.data[0]));
    }
  }

  selectRow = (row, e) => {
    const {setChosenRow, chosenRow} = this.props;
    if (e.target.nodeName === 'BUTTON' || e.target.nodeName === 'svg' || e.target.nodeName === 'INPUT') {
      return;
    }
    if (chosenRow === row) {
      setChosenRow('');
      return;
    }
    setChosenRow(row)
  };

  unSelectEvent = () => this.props.setChosen(null);

  formatDate = (date) => date ? moment(date).format('DD.MM.YYYY HH:mm') : 'N/A';

  getRowData = () => {
    const {data, chosen} = this.props;
    if (!chosen) {
      return [];
    }
    const chosenCondition = JSON.parse(chosen);
    const findIndex =  _.findIndex(data, chosenCondition);
    if (findIndex === -1) {
      return [];
    }
    if (data.length === 1) {
      return [
        {
          name: chosenCondition.conditionName,
          events: [...chosenCondition.events],
        }
      ]
    }
    if (findIndex === 0) {
      return [
        {
          name: chosenCondition.conditionName,
          events: [...chosenCondition.events],
        },
        {
          name: data[findIndex + 1].conditionName,
          events: [...data[findIndex + 1].events],
        },
      ]
    }
    if (findIndex === data.length - 1) {
      return [
        {
          name: data?.[findIndex - 1]?.conditionName,
          events: [...data?.[findIndex - 1]?.events],
        },
        {
          name: chosenCondition.conditionName,
          events: [...chosenCondition.events],
        },
      ]
    }
    return [
      {
        name: data?.[findIndex - 1]?.conditionName,
        events: [...data?.[findIndex - 1]?.events],
      },
      {
        name: chosenCondition.conditionName,
        events: [...chosenCondition.events],
      },
      {
        name: data[findIndex + 1].conditionName,
        events: [...data[findIndex + 1].events],
      },
    ]
  }

  openEditModal = (event) => {
    this.setState({isEditModalOpen: true});
    this.setState({selectedEvent: event});
  }
  closeEditModal = () => this.setState({isEditModalOpen: false});

  onActionClick = async (actionType, eventId) => {
    await http.put(`admin/event/${eventId}/${actionType}`);
    this.props.refetchData();
  }

  openConfirmationModal = (event) => {
    this.setState({confirmationModalOpen: true, deletedEvent: event});
  };

  closeConfirmationModal = () => {
    this.setState({confirmationModalOpen: false, deletedEvent: {}});
  };

  onDeleteClick = async (eventId) => {
    const { user } = this.props;
    if (user.role !== 'Admin') {
      const { data } = await http.delete(`events/fact/${eventId}`);
      if (data.error || data.info) {
        await confirmModal({
          type: data.error ? 'error' : 'info',
          data: data
        });
      }
    } else {
      const { data } = await http.delete(`admin/fact/${eventId}`)
      if (data.error || data.info) {
        await confirmModal({
          type: data.error ? 'error' : 'info',
          data: data
        });
      }
    }
    this.closeConfirmationModal();
    this.props.refetchData();
  }

  renderDeleteButton = (event) => {
    const DeleteButton = <S.ActionButton onClick={() => this.openConfirmationModal(event)}>Delete</S.ActionButton>;
    if (event.isAvailableForDelete) {
      return (
        <Fragment>
          {DeleteButton}
        </Fragment>
      )
    }
    return null;
  };

  renderActionButton = (event) => {
    const { user } = this.props;
    if (user.role !== 'Admin') {
      return null;
    }
    const RejectButton = <S.ActionButton onClick={() => this.onActionClick('reject', event.id)}>Reject</S.ActionButton>;
    const ApproveButton = <S.ActionButton onClick={() => this.onActionClick('approve', event.id)}>Approve</S.ActionButton>;
  
    if (event.statusId == 2) { // Completed
      return (
        <Fragment>
          {RejectButton}
          {ApproveButton}
        </Fragment>
      )
    }
    if (event.statusId == 3) { // Rejected
      return (
        <Fragment>
          {ApproveButton}
        </Fragment>
      )
    }
    if (event.statusId == 4) { // Approved
      return (
        <Fragment>
          {RejectButton}
        </Fragment>
      )
    }
    return null;
  }

  onDelete = () => {
    this.onDeleteClick(this.state.deletedEvent.id);
  };

  render() {
    const {data, isAdmin, setChosen, chosen, chosenRow, portOptions, cargoOptions, onAddPress, user, dateFrom, dateTo, eventStatuses} = this.props;
    const {isEditModalOpen, selectedEvent, confirmationModalOpen} = this.state;
    return (
      <S.Container>
        {
          confirmationModalOpen && (
            <ConfirmationModal
              isModalOpen={confirmationModalOpen}
              onRequestClose={this.closeConfirmationModal}
              onConfirm={this.onDelete}
              event={this.state.deletedEvent}
            />
          )
        }
        {isEditModalOpen && <EditModal
          isModalOpen={isEditModalOpen}
          onRequestClose={this.closeEditModal}
          event={selectedEvent}
          portOptions={portOptions}
          cargoOptions={cargoOptions}
          unSelectEvent={this.unSelectEvent}
          dateFrom={dateFrom}
          dateTo={dateTo}
          eventStatuses={eventStatuses}
        />}
        <S.LeftContainer>
         {data.map((item, index) => (
          <ContextMenuTrigger id={index}>
            {(JSON.stringify(item) === chosen && !isAdmin) ? <ContextMenu id={index} /> : null}
            <ListOption onAddPress={onAddPress} isAdmin={isAdmin} onItemPress={() => setChosen(JSON.stringify(item))} isActive={chosen === JSON.stringify(item)} item={item} />
         </ContextMenuTrigger>
         )
         )}
        </S.LeftContainer>
        <S.RightContainer>
        <S.Header rowsCount={user.role === 'Admin' ? 7 : 7}>
          <S.HeaderItem>Timestamp</S.HeaderItem>
          <S.HeaderItem>Event Type</S.HeaderItem>
          <S.HeaderItem>Port</S.HeaderItem>
          <S.HeaderItem>Cargo</S.HeaderItem>
          <S.HeaderItem>Comments</S.HeaderItem>
          <S.HeaderItem>Number of attachments</S.HeaderItem>
          <S.HeaderItem>Approval status</S.HeaderItem>
          <S.HeaderItem>Report</S.HeaderItem>
          {user.role === 'Basic' && <S.HeaderItem>Exclude</S.HeaderItem>}
          {user.role === 'Admin' && <S.HeaderItem>Actions</S.HeaderItem>}
        </S.Header>
        {chosen && this.getRowData().map((item, ind) => 
        <>
          <S.Separator>{item.name}</S.Separator>
          {item.events.map(event => 
          <> 
                <ContextMenuTrigger id={`${ind} + ${JSON.stringify(event)}`}>
                  {(JSON.stringify(event) === chosenRow && !isAdmin) ? <ContextMenu onOptionClick={() => this.openEditModal(event)} options={['Edit']} id={`${ind} + ${JSON.stringify(event)}`} /> : null}
                  <S.Row
                    onClick={(e) => this.selectRow(JSON.stringify(event), e)}
                    style={{
                      backgroundColor: `rgba(${approvalStatusesColors[event.statusId]})`,
                      border: JSON.stringify(event) === chosenRow ? '2px solid black' : '1px solid #e2e2e2',
                    }}
                    rowsCount={user.role === 'Admin' ? 7 : 7}
                    >
                    <S.RowItem>{this.formatDate(event.timestamp)}</S.RowItem>
                    <S.RowItem>{event.eventTypeName}</S.RowItem>
                    <S.RowItem>{event.port?.name}</S.RowItem>
                    <S.RowItem>
                      {event.cargo?.name}
                    </S.RowItem>
                    <S.RowItem>{event.comment}</S.RowItem>
                    <S.RowItem style={{
                      display: 'flex',
                      justifyContent: 'center'
                    }}>{event.numberOfAttachments}</S.RowItem>
                    <S.RowItem>{approvalStatuses[event.statusId]}</S.RowItem>
                    <S.RowItem></S.RowItem>
                    <S.ActionsButtonsContainer>
                      {user.role !== 'Admin' && <S.Checkbox checked={this.props.checked.includes(event.id)} onChange={() => this.props.onCheckboxChange(event)} type="checkbox" id="test" />}
                      {JSON.stringify(event) === chosenRow && this.renderDeleteButton(event)}
                      {JSON.stringify(event) === chosenRow && this.renderActionButton(event)}
                      {JSON.stringify(event) === chosenRow && event.reportAvailable && !event.reportId && user.role !== 'Admin' && <S.ActionButton onClick={() => this.props.history.push(`/reports/add/${event.id}`)} >Add</S.ActionButton>}
                      {JSON.stringify(event) === chosenRow && event.reportId && user.role !== 'Admin' && <S.ActionButton onClick={() => this.props.history.push(`/reports/edit/${event.reportId}`)} >Edit</S.ActionButton>}
                      {JSON.stringify(event) === chosenRow && event.reportId && user.role === 'Admin' && <S.ActionButton onClick={() => this.props.history.push(`/reports/edit/${event.reportId}`)} >View</S.ActionButton>}
                      {JSON.stringify(event) === chosenRow && user.role !== 'Admin' &&  <S.EditButton onClick={() => this.openEditModal(event)} ><EditIcon width={20} height={20} /></S.EditButton>}
                    </S.ActionsButtonsContainer>
                  </S.Row>
                  
                </ContextMenuTrigger>
              </>)}
        </>
              )}
          </S.RightContainer>
      </S.Container>
    )
  }
}

const mapStateToProps = ({auth}) => ({
  user: auth.user,
  authenticated: auth.authenticated,
});

export default connect(mapStateToProps, null)(withRouter(Table));