import styled from 'styled-components';

export const Container = styled.div`
  width: 95%;
  height: 62vh;
  background-color: white;
  display: flex;
  margin-top: 20px;
`;

export const RightContainer = styled.div`
  width: 80%;
  height: 100%;
  background-color: white;
  overflow-y: scroll;
`;

export const LeftContainer = styled.div`
  width: 20%;
  height: 100%;
  border-right: 1px solid #E5E5E5;
  overflow-y: scroll;
`;

export const Row = styled.div`
  display: grid;
  grid-template-columns: ${({rowsCount}) => `150px 150px repeat(${rowsCount}, 1fr)`};
  position: relative;
  border-bottom: 1px solid #e2e2e2;
  background-color: ${({isExpanded}) => isExpanded ? 'white' : 'white'};
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: ${({paddingLeft}) => paddingLeft ? paddingLeft : 30}px;
  align-items: center;
  cursor: pointer;
  span {
    color: ${({isExpanded}) => isExpanded ? 'black' : 'black'};
  }
`;

export const ActionsButtonsContainer = styled.div`
  position: absolute;
  right: 0;
  display: flex;
`;

export const EditButton = styled.button`
  width: 30px;
  height: 30px;
  margin-right: 5px;
`;

export const CreateButton = styled.button`
  width: 36px;
  height: 30px;
  position: absolute;
  right: 40px;
  text-align: center;
`;

export const ActionButton = styled.button`
  margin-right: 5px;
`;

export const RowItem = styled.span`
  flex: 1;
  /* color: black; */
  color: ${({isExpanded}) => isExpanded ? 'white' : 'black'};
  text-align: center;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const Header = styled.div`
  position: sticky;
  top: 0;
  background-color: white;
  /* z-index: 1111; */
  display: grid;
  grid-template-columns: ${({rowsCount}) => `150px 150px repeat(${rowsCount}, 1fr)`};
  border-bottom: 1px solid #e2e2e2;
  z-index: 11;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: ${({paddingLeft}) => paddingLeft ? paddingLeft : 30}px;
`;

export const HeaderItem = styled.span`
  flex: 1;
  color: black;
  font-weight: 700;
  text-align: center;
`;

export const Separator = styled.div`
  display: flex;
  justify-content: center;
  font-size: 14px;
  font-weight: 700;
`;

export const Checkbox = styled.input`
  background-color: red;
  width: 15px;
  height: 15px;
  margin-right: 5px;
`;
