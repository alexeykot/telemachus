import React from 'react';
import moment from 'moment';

import * as S from './styled';
import { connect } from 'react-redux';

const formatDate = (date) => moment(date).format('DD.MM.YYYY HH:mm');

const ListOption = (props) => {
  const {item} = props;
  return (
    <S.Container onClick={props.onItemPress} isActive={props.isActive}>
      {props.user.role === 'Basic' && <S.AddButton onClick={() => props.onAddPress(item.isCurrentCondition, item)}>Add</S.AddButton>}
      <S.OptionWrapper>
        <S.Label>Start date</S.Label>
        <S.Value>{item.startDate ? formatDate(item.startDate) : 'N/A'}</S.Value>
      </S.OptionWrapper>
      <S.OptionWrapper>
        <S.Label>End date</S.Label>
        <S.Value>{item.startDate ? formatDate(item.endDate) : 'N/A'}</S.Value>
      </S.OptionWrapper>
      <S.OptionWrapper>
        <S.Label>Condition</S.Label>
        <S.Value>{item.conditionName}</S.Value>
      </S.OptionWrapper>
      {!props.isAdmin ? <S.OptionWrapper>
        <S.Label>In progress quantity</S.Label>
        <S.Value>{item.inProgressEventsCount}</S.Value>
      </S.OptionWrapper> : <S.OptionWrapper>
        <S.Label>Vessel name</S.Label>
        <S.Value>{item.vesselName}</S.Value>
      </S.OptionWrapper>}
    </S.Container>
  )
}

const mapStateToProps = ({auth}) => ({
  user: auth.user,
});

export default connect(mapStateToProps, null)(ListOption);