import React from 'react';
import { connect } from 'react-redux';

import {requestLogout} from '../../store/auth';

import { ReactComponent as Usericon } from '../../assets/icons/user.svg';
import logo from '../../assets/images/compass.ico';
import * as S from './styled';
import { useHistory } from 'react-router-dom';

const Header = (props) => {
  const {user, requestLogout} = props;
  const history = useHistory();
  return (
    <S.Header>
    <S.LogoContainer onClick={() => history.push('/main')}>
      <S.Logo src={logo} />
      <S.Title>TELEMACHUS</S.Title>
    </S.LogoContainer>
    <S.UserContainer>
      <Usericon />
      <S.UserName>{user ? user.userName : ''}</S.UserName>
      <S.LogoutButton onClick={requestLogout}>LOGOUT</S.LogoutButton>
    </S.UserContainer>
  </S.Header>
  );
}

const mapStateToProps = ({auth}) => ({
  user: auth.user,
  authenticated: auth.authenticated,
});

const mapDispatchToProps = {
  requestLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
