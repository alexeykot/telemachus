import React from 'react';
import ReactDOM from 'react-dom';
import SnackbarProvider from 'react-simple-snackbar'
import ModalContainer from 'react-modal-promise'
import { HashRouter as Router } from 'react-router-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import UserStorage from './services/storage/user';

import './index.css';

import reportWebVitals from './reportWebVitals';
import AppRoute from './router/AppRoute';

const user = UserStorage.get();

const store = configureStore({
  auth: {
    authenticated: !!user,
    user,
  }
});

ReactDOM.render(
    <Provider store={store}>
      <SnackbarProvider>
        <React.StrictMode>
          <ModalContainer />
          <Router>
          <AppRoute />
          </Router>
        </React.StrictMode>
      </SnackbarProvider>
    </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
