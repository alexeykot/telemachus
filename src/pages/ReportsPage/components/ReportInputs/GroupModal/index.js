import React from 'react';
import Modal from 'react-modal';

import {ReactComponent as CloseIcon} from '../../../../../assets/icons/close.svg';

import _ from 'lodash';
import * as S from './styled';


const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: 'auto',
    width: 800,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'scroll',
    maxHeight: 400,
  }
};

const GroupModal = ({
  isModalOpen,
  onRequestClose,
  groups = [],
  groupName = '',
  onInputChange,
  values = [],
}) => {

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      style={customStyles}
      contentLabel="Example Modal"
    >
      <div style={{maxHeight: 600, overflowY: 'scroll'}}>
      <S.CloseButton onClick={onRequestClose}><CloseIcon /></S.CloseButton>
        <S.GroupName>Group: <strong>{groupName}</strong></S.GroupName>
        <div>
          {groups.map(group =>
          <span style={{display: 'flex'}}>
            <S.InputContainer>
              <S.Label>{group.name}</S.Label>
              <S.Input value={values.find(v => v?.id === group?.id)?.value || ''} onChange={e => onInputChange(e.target.value, group.id)} />
            </S.InputContainer>
            <div style={{display: 'flex'}}>
              {
                group?.subgroups?.length && group.subgroups.map(i => (
                  <S.InputContainer style={{marginLeft: 20}}>
                    <S.Label>{i.name}</S.Label>
                    <S.Input value={values.find(v => v?.id === i?.id)?.value || ''} onChange={e => onInputChange(e.target.value, i.id)} />
                  </S.InputContainer>
                ))
              }
            </div>
          </span>
          )}
        </div>
      </div>
  </Modal>
  )
}


export default GroupModal;
