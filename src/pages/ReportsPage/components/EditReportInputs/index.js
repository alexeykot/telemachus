import React from 'react';
import _ from 'lodash';

import * as S from './styled';
import http from '../../../../services/http';
import moment from 'moment';
import Loader from 'react-loader-spinner';
import GroupModal from './GroupModal';
import { useSelector } from 'react-redux';

const GROUP_NAMES = {
  0: 'ROB HFO ACTUAL',
  1: 'ROB HFO POOL',
  2: 'ROB MGO ACTUAL',
  3: 'ROB MGO POOL',
};

export const AUTO_GENERATED = [
  {
    label: 'DATE',
    field: 'timestamp',
  },
  {
    label: 'VOY',
    field: 'voyageId',
  },
  {
    label: 'PORT',
    field: 'port',
  },
  {
    label: 'TYPE',
    field: 'eventTypeName',
  },
  {
    label: 'BALLAST',
    field: 'ballast',
  },
]

const EditReportInputs = ({editReportsInputs, id, loading, event, date}) => {
  const [isModalOpen, setModalOpen] = React.useState(false);
  const [devidedInfo, setDevidedInfo] = React.useState([]);
  const [groupName, setGroupName] = React.useState('');

  const [inputsValue, setInputsValue] = React.useState([]);

  const grouped = _.groupBy(inputsValue.filter(i => i.group !== null), i => i.group);
  const reportsWithoutGroup = inputsValue.filter(i => i.group === null && !i.subgroupId);

  const user = useSelector(state => state.auth.user);

  React.useEffect(() => {
    setInputsValue(editReportsInputs);
  }, [editReportsInputs]);

  const onInputChange = (value, id) => {
    const findInputIndex = inputsValue.findIndex(i => i.fieldId === id);
    inputsValue[findInputIndex].value = value;
    setInputsValue([...inputsValue]);
  };

  const onSave = async () => {
    const formValues = inputsValue.map(i => ({
      FieldId: i.fieldId,
      Value: i.value,
    }));
    const payload = {
      FieldValues: [
        ...formValues,
      ]
    }
    await http.put(`/reports/${id}`, payload);
    alert('Saved');
  };

  const countGroupSum = (group) => {
    if (!inputsValue.length) return '';
    let sum = 0;
    group.forEach(g => {
      const find = inputsValue.find(v => v?.fieldId === g?.fieldId);
      if (find) {
        sum += parseFloat(find.value);
      }
    })
    if (sum === 0) {
      return ''
    } else {
      return sum.toFixed(2);
    }
  }

  const showDevidedInfo = (info, label) => {
    const formatted = info.map(i => {
      if (i.isSubgroupMainField) {
        const find = editReportsInputs.filter(inp => (inp.subgroupId === i.subgroupId) && !inp.isSubgroupMainField);
        if (find.length) {
          return {
            ...i,
            subgroups: find
          }
        }
      }
      return i;
    })
    setDevidedInfo(formatted);
    setModalOpen(true);
    setGroupName(label);
  }

  return (
    <S.Container>
      <GroupModal
        groupName={groupName}
        groups={devidedInfo}
        isModalOpen={isModalOpen}
        onRequestClose={() => {setModalOpen(false); setDevidedInfo([])}}
        onInputChange={onInputChange}
        values={inputsValue}
      />
      {loading && <div style={{display: 'flex', justifyContent: 'center'}}><Loader type="Oval" color="#77acd9" /></div>}
      {!loading && <div style={{display: 'flex', justifyContent: 'space-between'}}>
        <div>
          <h4>Event type: {event.eventTypeName}</h4>
          <h4 style={{marginBottom: 40}}>Timestamp: {moment(date).format('DD-MM-YYYY HH:mm')}</h4>
        </div>
        {user.role !== 'Admin' && <S.SaveButton onClick={onSave}>Save</S.SaveButton>}
      </div>}
      {!loading && <S.TableContainer>
        {AUTO_GENERATED.map(i => {
          return (
            <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', width: 100, paddingBottom: 20}}>
              <S.Label><strong>{i.label}</strong></S.Label>
              {i.field !== 'ballast' ? (i.field === 'timestamp' ? moment(date).format('DD-MM-YYYY HH:mm') : event[i.field]) : 'Ballast'}
            </div>
          )
        })}
        {Object.keys(grouped).map(group => {
          return (
            <div onClick={() => showDevidedInfo(grouped[group], GROUP_NAMES[group])} style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', width: 100, backgroundColor: '#e3ffea', cursor: 'pointer', paddingBottom: 20}}>
              <S.Label><strong>{GROUP_NAMES[group]}</strong></S.Label>
              <S.Value>{countGroupSum(grouped[group])}</S.Value>
            </div>
          )
        })}
        {_.sortBy(reportsWithoutGroup, (i) => i.fieldId).map(report => (
          <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between', width: 100, paddingBottom: 20}}>
          <S.Label><strong>{report.fieldName}</strong></S.Label>
          <S.Input disabled={user.role === 'Admin'} onChange={e => onInputChange(e.target.value, report.fieldId)} value={report.value} />
          </div>
        ))}
      </S.TableContainer>}
    </S.Container>
  )
}

export default EditReportInputs;
