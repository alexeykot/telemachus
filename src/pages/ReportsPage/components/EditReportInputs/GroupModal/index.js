import React from 'react';
import Modal from 'react-modal';

import {ReactComponent as CloseIcon} from '../../../../../assets/icons/close.svg';

import _ from 'lodash';
import * as S from './styled';
import { useSelector } from 'react-redux';


const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    height: 'auto',
    width: 800,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
  }
};

const GroupModal = ({
  isModalOpen,
  onRequestClose,
  groups = [],
  groupName = '',
  onInputChange,
  values = [],
}) => {

  const user = useSelector(state => state.auth.user);

  return (
    <Modal
      isOpen={isModalOpen}
      onRequestClose={onRequestClose}
      style={customStyles}
      contentLabel="Example Modal"
    >
      <div style={{maxHeight: 600, overflowY: 'scroll'}}>
      <S.CloseButton onClick={onRequestClose}><CloseIcon /></S.CloseButton>
        <S.GroupName>Group: <strong>{groupName}</strong></S.GroupName>
        <div>
          {groups.map(group =>
          <span style={{display: 'flex'}}>
            <S.InputContainer>
              <S.Label>{group.fieldName}</S.Label>
              <S.Input disabled={user.role === 'Admin'} value={values.find(v => v?.fieldId === group?.fieldId)?.value || ''} onChange={e => onInputChange(e.target.value, group.fieldId)} />
            </S.InputContainer>
            <div style={{display: 'flex'}}>
              {
                group?.subgroups?.length && group.subgroups.map(i => (
                  <S.InputContainer style={{marginLeft: 20}}>
                    <S.Label>{i.fieldName}</S.Label>
                    <S.Input disabled={user.role === 'Admin'} value={values.find(v => v?.fieldId === i?.fieldId)?.value || ''} onChange={e => onInputChange(e.target.value, i.fieldId)} />
                  </S.InputContainer>
                ))
              }
            </div>
          </span>
        )}
        </div>
      </div>
  </Modal>
  )
}


export default GroupModal;
