import styled from 'styled-components';

export const Container = styled.div`
  flex: 1;
  background-color: #EFEFEF;
  display: flex;
  flex-direction: column;
  padding: 0 20px;
`;

export const GroupContainer = styled.div`
  padding: 20px;
  border: 1px solid black;
  display: flex;
  flex-direction: row;
  width: min-content;
  margin-bottom: 20px;
  .input-container {
    margin-right: 20px;
  }
`;

export const InputsContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const GroupName = styled.p`
  font-weight: bold;
`;

export const Label = styled.p`
  font-weight: 500;
`;

export const Input = styled.input`
  width: 100%;
`;

export const SaveButton = styled.button`
  width: 200px;
  margin-top: 20px;
  height: 43px;
  background: #556080;
  border-radius: 4px;
  color: white;
`;

export const TableContainer = styled.div`
  display: grid;
  grid-auto-flow: column;  
  overflow: scroll;
  background-color: white;
  border-top: 1px solid black;
  border-right: 1px solid black;
  & > div {
    padding: 8px 4px;
    border-left: 1px solid black;
    border-bottom: 1px solid black;
  }
`;

export const Value = styled.p``;