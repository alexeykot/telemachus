import React from 'react';
import _, { isNumber } from 'lodash';
import moment from 'moment';

import * as S from './styled';
import { AUTO_GENERATED, TABLE_FIELDS } from './config';
import HistoryGroupModal from '../../../../components/HistoryGroupModal';
import ReactPaginate from 'react-paginate';
import { PAGE_SIZE } from '../../'


const GROUP_NAMES = {
  0: 'ROB HFO ACTUAL',
  1: 'ROB HFO POOL',
  2: 'ROB MGO ACTUAL',
  3: 'ROB MGO POOL',
};

const History = ({history, totalHistoryItems, handlePageClick, loading, editId}) => {

  const [isModalOpen, setModalOpen] = React.useState(false);
  const [devidedInfo, setDevidedInfo] = React.useState([]);
  const [groupName, setGroupName] = React.useState('');

  const sum = (array, key) => {
    const sumed = _.sumBy(array, (i) => {
      return +i.value;
    });
    if (isNaN(sumed)) {
      return 0;
    }
    return sumed.toFixed(2);
  }

  const showDevidedInfo = (info, label) => {
    setDevidedInfo(info);
    setModalOpen(true);
    setGroupName(label);
  }

  const getHeaders = (history) => {
    const itemsWithoutGroup = history[0].reportFields.filter(i => i.group === null).filter(i => i.isSubgroupMainField !== false);
    const itemsWithGroup = history[0].reportFields.filter(i => i.group !== null);
    const groupedHeaders = Object.values(grouped(itemsWithGroup)).map(i => i[0].fieldName.split('#')[0]);
    const notGroupedHeaders = itemsWithoutGroup.map(i => i.fieldName);
    const notGroupedIds = itemsWithoutGroup.map(i => i.fieldId);
    return {
      notGroupedHeaders,
      groupedHeaders,
      notGroupedIds,
    }
  }

  const getNotGroupItems = (reportFields, notGroupedIds) => {
    const filtered = reportFields.filter(i => i.group === null);
    const finall = filtered.filter(i => notGroupedIds.includes(i.fieldId))
    return finall;
  }

  const getGroupItems = (reportFields) => {
    const fieldsWithGroup = reportFields.filter(i => i.group !== null);
    const groupedByGroup = grouped(fieldsWithGroup);

    let finallGroupResult = [];
    for (let i = 0; i < Object.keys(groupedByGroup).length; i++) {
      let key = Object.keys(groupedByGroup)[i];
      let valuesSum = sum(groupedByGroup[key], 'value');

      const withSubgroups = groupedByGroup[key].map(i => {
        if (i.isSubgroupMainField) {
          const find = reportFields.filter(inp => !inp.isSubgroupMainField && inp.subgroupId === i.subgroupId);
          if (find.length) {
            return {
              ...i,
              subgroups: find,
            }
          }
        }
        return i;
      });
    
      finallGroupResult.push({
        fieldName: GROUP_NAMES[key],
        value: valuesSum,
        isGrouped: true,
        groups: withSubgroups,
      })
    }
    return finallGroupResult;
  };

  const grouped = (items) => _.groupBy(items, i => i.group);

  return (
    <S.Container>
      <HistoryGroupModal
        groupName={groupName}
        groups={devidedInfo}
        isModalOpen={isModalOpen}
        onRequestClose={() => {setModalOpen(false); setDevidedInfo([])}}
      />
      {history.length ?
        <>
          <S.TableContainer>
            {AUTO_GENERATED.map(field => <span><strong>{field.label}</strong></span>)}
            {
              ['ROB HFO ACTUAL', 'ROB HFO POOL', 'ROB MGO ACTUAL', 'ROB MGO POOL'].map(i => <span><strong>{i}</strong></span>)
            }
            {getHeaders(history).notGroupedHeaders.map(i => <span><strong>{i}</strong></span>)}
            {history.filter(i => i.reportFields.length).map(historyItem => {
              return (
                <>
                  <span style={historyItem.id === editId ? {backgroundColor: '#2ab7f5'} : {}}>{moment(historyItem.date).format('DD-MM-YYYY HH:mm')}</span>
                  <span style={historyItem.id === editId ? {backgroundColor: '#2ab7f5'} : {}}>{historyItem.event.voyageId}</span>
                  <span style={historyItem.id === editId ? {backgroundColor: '#2ab7f5'} : {}}>{historyItem.event.port}</span>
                  <span style={historyItem.id === editId ? {backgroundColor: '#2ab7f5'} : {}}>{historyItem.event.eventTypeName}</span>
                  <span style={historyItem.id === editId ? {backgroundColor: '#2ab7f5'} : {}}>Ballast</span>
                  {getGroupItems(historyItem.reportFields).map(i => <S.ClickableItem  onClick={() => showDevidedInfo(i.groups, i.fieldName)} style={{}}>{i.value}</S.ClickableItem>)}
                  {getNotGroupItems(historyItem.reportFields, getHeaders(history).notGroupedIds).map(i => <span style={historyItem.id === editId ? {backgroundColor: '#2ab7f5'} : {}}>{i.value}</span>)}
                </>
              )
            })}
          </S.TableContainer>
          
        </>
      : <>{!loading ? <S.Title>No history</S.Title> : null}</>}
          {totalHistoryItems ? <ReactPaginate
            previousLabel={'previous'}
            nextLabel={'next'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={totalHistoryItems ? Math.ceil(totalHistoryItems / PAGE_SIZE) : 1}
            onPageChange={handlePageClick}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            containerClassName={'pagination'}
            subContainerClassName={'pages pagination'}
            activeClassName={'active'}
          /> : null}
    </S.Container>
  )
}

export default History;
