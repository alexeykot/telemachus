import styled from 'styled-components';

export const Container = styled.div`
  flex: 1;
  background-color: #EFEFEF;
  display: flex;
  flex-direction: column;
  padding: 0 20px;
  padding-bottom: 80px;
  margin-top: 20px;
`;

export const TableContainer = styled.div`
  display: grid;
  background-color: white;
  grid-template-columns: repeat(26, 1fr);
  overflow: scroll;
  border-top: 1px solid black;
  border-right: 1px solid black;
  & > span {
    width: 100px;
    padding: 8px 4px;
    border-left: 1px solid black;
    border-bottom: 1px solid black;
  }
`;

export const Title = styled.h3`

`;

export const ClickableItem = styled.span`
  cursor: pointer;
  background-color: #e3ffea;
  :hover {
    background-color: #02a32a;
  }
`;
