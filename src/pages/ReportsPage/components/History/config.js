
export const AUTO_GENERATED = [
  {
    label: 'DATE',
    field: '',
  },
  {
    label: 'VOY',
    field: '',
  },
  {
    label: 'PORT',
    field: '',
  },
  {
    label: 'TYPE',
    field: '',
  },
  {
    label: 'BALLAST',
    field: '',
  },
]
export const TABLE_FIELDS = [
  {
    label: 'Instructed speed',
    field: '',
  },
  {
    label: 'Instructed Consumption (mt/24h)',
    field: '',
  },
  {
    label: 'Calculated Pool Index at Instructed Speed',
    field: '',
  },
  {
    label: 'ROB HFO ACTUAL',
    field: '',
  },
  {
    label: 'ROB HFO POOL',
    field: '',
  },
  {
    label: 'ROB MGO ACTUAL',
    field: '',
  },
  {
    label: 'ROB MGO POOL',
    field: '',
  },
  {
    label: 'Distance to Go',
    field: '',
  },
  {
    label: 'Distance over ground',
    field: '',
  },
  {
    label: 'Slip Actual',
    field: '',
  },

  {
    label: 'HFO Period Hours',
    field: '',
  },
  {
    label: 'MGO Period Hour',
    field: '',
  },
  {
    label: 'HFO ACTUAL M/E CONS',
    field: '',
  },
];
