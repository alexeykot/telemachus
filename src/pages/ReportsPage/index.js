import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Header from '../../components/Header';
import http from '../../services/http';
import EditReportInputs from './components/EditReportInputs';
import History from './components/History';
import ReportInputs from './components/ReportInputs';

import * as S from './styled';
import { Fragment } from 'react';

export const PAGE_SIZE = 5;

class ReportsPage extends React.Component {

  state = {
    reportsInputs: [],
    history: [],
    totalHistoryItems: 0,
    editReportsInputs: [],
    loading: false,
    selectedHistoryPage: 1,
    event: {},
    date: '',
    editId: '',
  }
  async componentDidMount() {
    const {type} = this.props.match.params;
    this.setState({loading: true});
    if (type !== 'edit') {
      const {data: reportsInputsData} = await http.get(`/reports/${this.props.match.params.id}/fields`);
      this.setState({reportsInputs: reportsInputsData })
    } else {
      const {data: editReportsInputs} = await http.get(`/reports/${this.props.match.params.id}`);
      this.setState({ editReportsInputs: editReportsInputs.reportFields, event: editReportsInputs.event, date: editReportsInputs.date, editId: editReportsInputs.id});
    }
    const {data: historyData} = await http.get(`/reports/history?page=${this.state.selectedHistoryPage}&pageSize=${PAGE_SIZE}`);
    const filteredHistoryData = historyData.items.filter(i => i.reportFields.length);

    const newItems = filteredHistoryData.map(i => ({
      ...i,
      reportFields: _.sortBy(i.reportFields, i => i.fieldId),
    }))
    const formattedData = [...newItems];
  
    this.setState({ history: formattedData, totalHistoryItems: historyData.totalCount })
    this.setState({loading: false});
  }

  handlePageClick = async (data) => {
    let selected = data.selected;
    this.setState({selectedHistoryPage: selected + 1});
    const {data: historyData} = await http.get(`/reports/history?page=${selected + 1}&pageSize=5`);
    this.setState({ history: historyData.items, totalHistoryItems: historyData.totalCount })
  };

  render() {
    const {type} = this.props.match.params;
    return (
      <S.Container>
        <Header />
        <S.Backbutton onClick={() => this.props.history.push('/main')}>Go Back</S.Backbutton>
        {type === 'add' && <ReportInputs loading={this.state.loading} id={this.props.match.params.id} reportsInputs={this.state.reportsInputs} />}
        {type === 'edit' && (
          <EditReportInputs
            date={this.state.date}
            event={this.state.event}
            loading={this.state.loading}
            id={this.props.match.params.id}
            editReportsInputs={this.state.editReportsInputs}
          />
        )}
        {this.props.user.role !== 'Admin' ? 
          <Fragment>
            <S.Separator />
            <h1 style={{paddingLeft: 20}}>History</h1>
            <History
              editId={this.state.editId}
              handlePageClick={this.handlePageClick}
              totalHistoryItems={this.state.totalHistoryItems}
              loading={this.state.loading}
              history={this.state.history}
            />
          </Fragment>
        : null}
      </S.Container>
    )
  }
}

const mapStateToProps = ({auth}) => ({
  user: auth.user,
  authLoading: auth.authLoading,
  authenticated: auth.authenticated,
});

export default withRouter(connect(mapStateToProps, null)(ReportsPage));
