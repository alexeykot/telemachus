import styled from 'styled-components';

export const Container = styled.div`
  background-color: #EFEFEF;
  min-height: 100vh;
`;

export const Separator = styled.div`
  width: 100%;
  height: 1px;
  background-color: black;
  margin-top: 20px;
`;

export const Backbutton = styled.div`
  height: 43px;
  width: 117px;
  background: #556080;
  border-radius: 4px;
  color: white;
  margin-bottom: 20px;
  margin-left: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  :hover {
    opacity: 0.5;
    cursor: pointer;
  }
`;
