export const textInputs =  [
  {
    placeholder: 'User Name',
    type: "text",
    inputType: 'UserName',
  },
  {
    placeholder: 'Password',
    type: "password",
    inputType: 'Password',
  },
];
