import styled from 'styled-components';

export const Container = styled.div`
  height: 300px;
  width: 500px;
  background-color: #f0f5bc;
  border-radius: 10px;
  box-shadow: 16px 11px 13px 5px rgba(0,0,0,0.57);
  border: 2px solid black;
`;

export const Title = styled.p`
  color: black;
`;

export const Icon = styled.img`
  width: 50px;
  height: 50px;
`;

export const TitleRow = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;
`;

export const FormContainer = styled.div`
  padding: 0 10px 0 80px;
  margin-top: 20px;
`;

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-right: 30px;
  margin-top: 30px;
`;

export const Button = styled.button`
  height: 30px;
  width: 70px;
  margin-left: 15px;
  color: black;
  font-size: 14px;
`;