import React from 'react';

import logo from '../../../../assets/images/compass.ico';
import TextInput from '../TextInput';
import { textInputs } from './config';
import * as S from './styled';

const LoginForm = (props) => {
  const [credentials, setCredentials] = React.useState({
    UserName: '',
    Password: '',
  });
  const onInputChange = (value, type) => setCredentials({
    ...credentials,
    [type]: value,
  });
  const onSubmit = () => props.requestSignIn(credentials);
  const {authLoading, isError} = props;
  return (
    <S.Container>
      <S.TitleRow>
        <S.Icon src={logo} className="App-logo" alt="logo" />
        <S.Title>Please Login To Access Telemachus</S.Title>
      </S.TitleRow>
      <S.FormContainer>
          {textInputs.map(input => (
            <TextInput
              isError={isError}
              key={input.inputType}
              onInputChange={onInputChange}
              inputType={input.inputType}
              placeholder={input.placeholder}
              type={input.type}
            />
          ))}
        </S.FormContainer>
      <S.ButtonContainer>
        <S.Button disabled={authLoading} onClick={onSubmit}>Login</S.Button>
        <S.Button disabled={authLoading}>Cancel</S.Button>
      </S.ButtonContainer>
    </S.Container>
  )
}

export default LoginForm;
