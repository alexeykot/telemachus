import styled from 'styled-components';

export const Container = styled.div`
  height: 40px;
  margin-top: 10px;
`;

export const InputContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Placeholder = styled.p`
  font-size: 15px;
  color: black;
`;

export const Input = styled.input`
  width: 200px;
  color: black;
  font-size: 14px;
  border-width: ${({isError}) => isError ? 2 : 1}px;
  border-color: ${({isError}) => isError ? 'red' : 'black'};
`;

export const Hint = styled.p`
  color: red;
  font-size: 12px;
`;