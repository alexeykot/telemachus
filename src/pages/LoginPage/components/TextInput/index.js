import React from 'react';

import * as S from './styled';

const TextInput = ({placeholder, type, inputType, onInputChange, isError}) => {
  return (
    <S.Container>
      <S.InputContainer>
        <S.Placeholder>{placeholder}</S.Placeholder>
        <S.Input isError={isError} onChange={(e) => onInputChange(e.target.value, inputType)} type={type} />
      </S.InputContainer>
    </S.Container>
  )
}

export default TextInput;
