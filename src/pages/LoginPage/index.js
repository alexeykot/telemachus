import React from 'react';
import {Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import { withSnackbar } from 'react-simple-snackbar'

import {requestSignIn} from '../../store/auth';
import LoginForm from './components/LoginForm';

import * as S from './styled';

class LoginPage extends React.Component {
  componentDidUpdate(prevProps) {
    if ((prevProps.authenticated !== this.props.authenticated) && this.props.authenticated) {
      this.props.history.push('/main');
    }
    if ((prevProps.authError !== this.props.authError) && this.props.authError) {
      const { openSnackbar } = this.props
      openSnackbar('You entered invalid credentials!', 3000);
    }
  }
  render() {
    return (
      <>
        {!this.props.authenticated ? <S.Container>
          <LoginForm {...this.props} />
        </S.Container> : <Redirect to="/main" />}
      </>
    )
  }
}

const mapStateToProps = ({auth}) => ({
  user: auth.user,
  authLoading: auth.authLoading,
  authenticated: auth.authenticated,
  authError: auth.authError,
});

const mapDispatchToProps = {
  requestSignIn,
};

const options = {
  position: 'top-center',
  style: {
    backgroundColor: 'red',
    color: 'white',
  }
}

export default withSnackbar(connect(mapStateToProps, mapDispatchToProps)(LoginPage), options);
