import React from 'react';
import { connect } from 'react-redux';

import { requestGetConditionOptions } from '../../store/reports';
import Conditions from '../../components/Conditions';
import Header from '../../components/Header';

import * as S from './styled';

class MainPage extends React.Component {

  componentDidMount() {
    this.props.requestGetConditionOptions();
  }

  render() {
    return (
      <S.Container>
        <Header />
        <Conditions />
      </S.Container>
    )
  }
}

const mapStateToProps = ({auth}) => ({
  user: auth.user,
  authLoading: auth.authLoading,
  authenticated: auth.authenticated,
});

const mapDispatchToProps = {
  requestGetConditionOptions,
}
export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
