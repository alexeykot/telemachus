## Getting Started

In the project directory, you can run:

### `yarn install`

then

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

To modify API url you need go to:

### `src/services/http/index.js`
and modify
### `const baseURL = '...'`